package pt.tmg.dicoogle.web.essentials;

import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import pt.ua.dicoogle.sdk.utils.TagValue;
import pt.ua.dicoogle.sdk.utils.TagsStruct;

/**
 * Simple Helloworld servlet
 * 
 * @author Tiago Marques Godinho, tmgodinho@ua.pt
 *
 */
public class TagsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		TagsStruct tagStruct = TagsStruct.getInstance();

		TreeSet<TagValue> tags = new TreeSet<TagValue>(new TagValueComparator());
		tags.addAll(tagStruct.getAllFields());
		
		Set<TagValue> dimFields = tagStruct.getDIMFields();
		Set<TagValue> privateFields = tagStruct.getPrivateFields();
		
		
		Document jdomDoc = new Document();
		Element rootElement = new Element("NativeDicomModel");
		jdomDoc.setRootElement(rootElement);
		for (TagValue t : tags) {
			Element tagElem = tagValueToXML(t);
			if(dimFields.contains(t))
				tagElem.setAttribute("DIM", "");
			else if (privateFields.contains(t))
				tagElem.setAttribute("PrivateCreator", "Unknown");
			rootElement.addContent(tagElem);
		}
		
		resp.setStatus(HttpStatus.OK_200);

		XMLOutputter xml = new XMLOutputter();
		xml.setFormat(Format.getPrettyFormat());
		xml.output(jdomDoc, resp.getOutputStream());

		resp.getOutputStream().flush();
	}
	
	private static Element tagValueToXML(TagValue t){
		Element tagElem = new Element("DicomAttribute");
		tagElem.setAttribute("tag", t.getTagID());
		tagElem.setAttribute("vr", t.getVR());
		tagElem.setAttribute("keyword", t.getAlias());
		return tagElem;
	}

	// <NativeDicomModel>
	// <DicomAttribute tag="0020000D" vr="UI" keyword="StudyInstanceUID">
	// <Value
	// number="1">1.2.444.200036.9116.2.2.2.1762893313.1029997326.945876</Value>
	// </DicomAttribute>
	// </NativeDicomModel>

	private static class TagValueComparator implements Comparator<TagValue>{

		public int compare(TagValue o1, TagValue o2) {
			long o1l = Long.parseLong(o1.getTagID(), 16) - Long.parseLong(o2.getTagID(), 16) ;
			
			if(o1l < 0)
				return -1;
			if(o1l > 0)
				return 1;
			return 0;
		}
		
	}
	
}
