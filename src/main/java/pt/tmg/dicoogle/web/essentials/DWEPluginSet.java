package pt.tmg.dicoogle.web.essentials;

import net.xeoh.plugins.base.annotations.PluginImplementation;
import pt.ua.dicoogle.sdk.PluginBase;
import pt.ua.dicoogle.sdk.core.DicooglePlatformInterface;

/**
 * Dicoogle Whole Slide Imaging plugin set
 * 
 * @author Tiago Marques Godinho, tmgodinho@ua.pt
 *
 */
@PluginImplementation
public class DWEPluginSet extends PluginBase {

	@Override
	public void setPlatformProxy(DicooglePlatformInterface core) {
		// TODO Auto-generated method stub
		super.setPlatformProxy(core);
	}

	public static DicooglePlatformInterface core = null;

	public DWEPluginSet() {
		super();
		this.jettyPlugins.add(new WebServices());
	}

	@Override
	public String getName() {
		return "tmg_dicoogle-web-essentials";
	}
}
