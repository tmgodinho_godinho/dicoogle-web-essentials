package pt.tmg.dicoogle.web.essentials;

import org.apache.commons.configuration.XMLConfiguration;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import pt.ua.dicoogle.sdk.JettyPluginInterface;
import pt.ua.dicoogle.sdk.settings.ConfigurationHolder;

/**
 * Web-services Plugin
 * 
 * @author Tiago Marques Godinho, tmgodinho@ua.pt
 *
 */
public class WebServices implements JettyPluginInterface {

	private ConfigurationHolder settings;
	
	public String getName() {
		return "tmg_dicoogle-web-essentials_webservices";
	}

	public boolean enable() {
		return true;
	}

	public boolean disable() {
		return false;
	}

	public boolean isEnabled() {
		return true;
	}

	@SuppressWarnings("unused")
	public void setSettings(ConfigurationHolder settings) {
		this.settings = settings;
		XMLConfiguration cnf = this.settings.getConfiguration();
		boolean forceSave = false;
	}

	public ConfigurationHolder getSettings() {
		return settings;
	}

	public HandlerList getJettyHandlers() {

		ServletContextHandler handler = new ServletContextHandler();
		handler.setContextPath("/web-essentials");
		handler.addServlet(new ServletHolder(new HelloWorldServlet()), "/hello");
		handler.addServlet(new ServletHolder(new TagsServlet()), "/tags");

		HandlerList l = new HandlerList();
		l.addHandler(handler);

		return l;
	}

}
